 # Copyright 2019 Hewlett Packard Enterprise Development LP
 #
 # Licensed under the Apache License, Version 2.0 (the "License"); you may
 # not use this file except in compliance with the License. You may obtain
 # a copy of the License at
 #
 #      http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 # WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 # License for the specific language governing permissions and limitations
 # under the License.

# -*- coding: utf-8 -*-
"""
An example of getting the resource directory for HPE iLO systems
"""

import sys
from .config import Config
from redfish import RedfishClient
from redfish.rest.v1 import ServerDownOrUnreachableError

def get_resource_directory(redfishobj):
    get_resource_directory.resource_list = vars(get_resource_directory).setdefault('resource_list', None)
    if get_resource_directory.resource_list:
        return get_resource_directory.resource_list

    try:
        resource_uri = redfishobj.root.obj.Oem.Hpe.Links.ResourceDirectory['@odata.id']
    except KeyError:
        sys.stderr.write("Resource directory is only available on HPE servers.\n")
        sys.exit(1)
        return None

    response = redfishobj.get(resource_uri)
    resource_list = []

    if response.status == 200:
        #sys.stdout.write("\tFound resource directory at /redfish/v1/resourcedirectory" + "\n\n")
        resource_list = response.dict["Instances"]
    else:
        sys.exit(2)

    get_resource_directory.resource_list = resource_list

    return resource_list

def list_resources():
    client = Config()
    client.parse_args(sys.argv)
    REDFISH_OBJ = client.get_client()
    resources = get_resource_directory(REDFISH_OBJ)

    for resource in resources:
        try:
            sys.stdout.write("\t" + str(resource["@odata.type"]) + \
                             "\n\t\t" + str(resource["@odata.id"]) + "\n")
        except KeyError:
            pass

    client.logout()

if __name__ == "__main__":
    list_resources()