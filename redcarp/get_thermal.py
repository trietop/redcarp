# Copyright 2019 Hewlett Packard Enterprise Development LP
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

# -*- coding: utf-8 -*-
"""
An example of gathering the power metrics average on HPE iLO systems
"""

import sys
import json
from .config import Config
from redfish import RedfishClient
from redfish.rest.v1 import ServerDownOrUnreachableError

from .get_resource_directory import get_resource_directory


def get_thermal(_redfishobj):

    thermal_uri = None
    resource_instances = get_resource_directory(_redfishobj)

    for instance in resource_instances:
        # Use Resource directory to find the relevant URI
        if '#Thermal.' in instance['@odata.type']:
            thermal_uri = instance['@odata.id']
            break

    if thermal_uri:
        thermal_data = _redfishobj.get(thermal_uri)
    return thermal_data


def capture_thermal():
    client = Config()
    client.parse_args(sys.argv)
    REDFISH_OBJ = client.get_client()
    thermal_data = get_thermal(REDFISH_OBJ)
    print(json.dumps(thermal_data.obj, indent=4, sort_keys=True))
    client.logout()


if __name__ == "__main__":
    capture_thermal()
