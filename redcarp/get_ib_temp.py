
# -*- coding: utf-8 -*-
"""
Capture the temperature from the Mellanox IB Card
"""

import argparse
import sys
import json
import re
import subprocess
import requests
from flask import Flask

from .config import Config

api = Flask(__name__)


def get_mst_devices():
    out = subprocess.Popen(['/usr/bin/mst', 'status'],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT)
    stdout, stderr = out.communicate()
    return re.finditer('(/dev/\S+)', stdout.decode("ascii"))


def get_mst_temp(dev):
    out = subprocess.Popen(['/usr/bin/mget_temp', '-d', dev],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT)
    stdout, stderr = out.communicate()
    return stdout.decode("ascii").strip()


def get_mlx_link(dev):
    out = subprocess.Popen(['/usr/bin/mlxlink', '-d', dev, '-m', '-c', '-e', '--show_serdes_tx', '--show_device', '--show_ber_monitor', '--show_external_phy'],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT)
    stdout, stderr = out.communicate()
    ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
    stripped = ansi_escape.sub('', stdout.decode("ascii"))
    return re.finditer('(.*):\s+(.*)', stripped)


def get_mlx_info():
    mlxinfo = []
    for dev in get_mst_devices():
        info = {}
        device = dev.group(0)
        info['device'] = device
        info['temp'] = get_mst_temp(device)
        for kv in get_mlx_link(device):
            kvk = kv.group(1).strip()
            kvv = kv.group(2).strip()
            if len(kvk) > 0 and kvv != "N/A":
                info[kvk] = kvv
        mlxinfo.append(info)
    return {"Devices": mlxinfo}


def get_mlx_remote(url):
    url = url + '/mlx' if not url.endswith('/mlx') else url
    r = requests.get(url)
    return r.json()


@api.route('/mlx', methods=['GET'])
def get_mlx_json():
    return json.dumps(get_mlx_info())


def add_args(parser):
    parser.add_argument("-s", "--serve", action='store_true',
                        help="Start webserver")
    parser.add_argument("-p", "--port", type=int,
                        default=9080, help="Serve on port")
    parser.add_argument("-b", "--bind", default="0.0.0.0",
                        help="Bind to address")


def capture_mlx():
    parser = argparse.ArgumentParser()
    add_args(parser)
    args = parser.parse_args()

    if args.serve:
        api.run(host=args.bind, port=args.port)
    else:
        mlxinfo = get_mlx_info()
        print(json.dumps(mlxinfo, indent=4, sort_keys=True))


if __name__ == "__main__":
    capture_mlx()
