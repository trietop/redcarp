import signal, sys, time
import csv, json
import datetime
from jsonpath_ng.ext import parse

from .config import Config

from .capture import collect

def signal_handler(sig, frame):
    sys.exit(0)

def add_args(parser):
    parser.add_argument("-c", "--count", type=int, default=1, help="Collect <x> samples")
    parser.add_argument("-l", "--collect", action='store_true', help="Continuously collect samples")
    parser.add_argument("-r", "--repeat", type=int, default=5, help="Repeat every <n> seconds")

def output_header(writer, data):
    # Headers
    header_line = { "mellanox": [], "temperature": [], "fans": [] }
    # Mellanox
    header_line["mellanox"] = [match.value for match in parse('$.mellanox.Devices[*].device').find(data)]
    # Temperature
    header_line["temperature"] = [match.value for match in parse('$.thermal.Temperatures[*].Name').find(data)]
    # Fans
    header_line["fans"] = [match.value for match in parse('$.thermal.Fans[*].Name').find(data)]

    # Print Arrays
    writer.writerow(["datetime"] + header_line["mellanox"] + header_line["temperature"] + header_line["fans"])
    return header_line

def output(writer, data, args):
    output.header = vars(output).setdefault('header', None)
    output.header_count = vars(output).setdefault('header_count', 0)
    if not output.header:
        output.header = output_header(writer, data)
        output.header_count = 1 + len(output.header["mellanox"]) + len(output.header["temperature"]) + len(output.header["fans"])
    
    csv_row = [''] * output.header_count
    csv_row[0] = datetime.datetime.utcnow().isoformat()
    idx_len = 1

    for row in [match.value for match in parse('$.mellanox.Devices[*]').find(data)]:
        loc = output.header["mellanox"].index(row["device"])
        if loc < 0:
            sys.stderr.write("Unknown mellanox device: " + row["device"] + "\n")
            continue
        csv_row[idx_len + loc] = row["temp"]
    idx_len += len(output.header["mellanox"])

    for row in [match.value for match in parse('$.thermal.Temperatures[*]').find(data)]:
        loc = output.header["temperature"].index(row["Name"])
        if loc < 0:
            sys.stderr.write("Unknown temperature sensor: " + row["Name"] + "\n")
            continue        
        csv_row[idx_len + loc] = row["ReadingCelsius"]
    idx_len += len(output.header["temperature"])

    for row in [match.value for match in parse('$.thermal.Fans[*]').find(data)]:
        loc = output.header["fans"].index(row["Name"])
        if loc < 0:
            sys.stderr.write("Unknown fan sensor: " + row["Name"] + "\n")
            continue 
        csv_row[idx_len + loc] = row["Reading"]

    if len(csv_row) != output.header_count:
        sys.stderr.write("Number of values do not match header: " + str(len(csv_row)) + " != " + str(output.header_count) + "\n")
    writer.writerow(csv_row)


def as_csv():
    signal.signal(signal.SIGINT, signal_handler)

    client = Config()
    args = client.parse_args(sys.argv, add_args)
    REDFISH_OBJ = client.get_client()

    iterations = args.count
    writer = csv.writer(sys.stdout, delimiter=',')

    while args.collect or iterations > 0:
        iterations -= 1
        data = collect(args, REDFISH_OBJ)
        output(writer, data, args)
        if args.collect or iterations > 0:
            time.sleep(args.repeat)

    client.logout()

if __name__ == "__main__":
    as_csv()