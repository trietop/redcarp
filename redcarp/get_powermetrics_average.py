# Copyright 2019 Hewlett Packard Enterprise Development LP
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

# -*- coding: utf-8 -*-
"""
An example of gathering the power metrics average on HPE iLO systems
"""

import sys
import json

from redfish import RedfishClient
from redfish.rest.v1 import ServerDownOrUnreachableError

from .config import Config
from .get_resource_directory import get_resource_directory


def get_powermetrics_average(_redfishobj):
    power_metrics_uri = None

    resource_instances = get_resource_directory(_redfishobj)
    for instance in resource_instances:
        # Use Resource directory to find the relevant URI
        if '#Power.' in instance['@odata.type']:
            power_metrics_uri = instance['@odata.id']
            break

    if power_metrics_uri:
        power_metrics_data = _redfishobj.get(power_metrics_uri)
    return power_metrics_data


def capture_power():
    client = Config()
    client.parse_args(sys.argv)
    REDFISH_OBJ = client.get_client()
    power_metrics_data = get_powermetrics_average(REDFISH_OBJ)
    print(json.dumps(power_metrics_data.obj, indent=4, sort_keys=True))
    client.logout()


if __name__ == "__main__":
    capture_power()
