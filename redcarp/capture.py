import signal, sys, time
import json

from .config import Config

from .get_ib_temp import get_mlx_info, get_mlx_remote
from .get_powermetrics_average import get_powermetrics_average
from .get_thermal import get_thermal

def signal_handler(sig, frame):
    sys.exit(0)

def add_args(parser):
    parser.add_argument("-f", "--format", action='store_true', help="Format output")
    parser.add_argument("-c", "--count", type=int, default=1, help="Collect <x> samples")
    parser.add_argument("-l", "--collect", action='store_true', help="Continuously collect samples")
    parser.add_argument("-o", "--output", help="Store output to directory")
    parser.add_argument("-r", "--repeat", type=int, default=5, help="Repeat every <n> seconds")

def collect(args, _redfish):
    thermal_data = get_thermal(_redfish)
    power_data = get_powermetrics_average(_redfish)
    if args.mlx:
        mlx_data = get_mlx_remote(args.mlx)
    else:
        mlx_data = get_mlx_info()
    return { 'thermal': thermal_data.dict, 'power': power_data.dict, 'mellanox': mlx_data }

def output(data, args):
    if args.output:
        timestr = time.strftime("%Y%m%d_%H%M%S")
        with open(args.output + '/capture_' + timestr + '.json', 'w') as outfile:
            json.dump(data, outfile)
    else:
        if args.format:
            print('---')
            print(json.dumps(data, indent=4, sort_keys=True))
        else:
            print(json.dumps(data))

def as_json():
    signal.signal(signal.SIGINT, signal_handler)

    client = Config()
    args = client.parse_args(sys.argv, add_args)
    REDFISH_OBJ = client.get_client()

    iterations = args.count

    while args.collect or iterations > 0:
        iterations -= 1
        data = collect(args, REDFISH_OBJ)
        output(data, args)
        if (args.collect or iterations > 0) and args.repeat > 0:
            time.sleep(args.repeat)

    client.logout()

if __name__ == "__main__":
    as_json()