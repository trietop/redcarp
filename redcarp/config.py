
# -*- coding: utf-8 -*-
"""
Default connection configuration for capture scripts
"""

import argparse
import json
import sys
from redfish import RedfishClient
from redfish.rest.v1 import ServerDownOrUnreachableError, InvalidCredentialsError


class Config(object):
    def __init__(self):
        self.connection = None
        self.system_url = None
        self.login_account = None
        self.login_password = None

    def get_client(self):
        self.logout()
        try:
            self.connection = RedfishClient(
                base_url=self.system_url, username=self.login_account, password=self.login_password)
            self.connection.login()
        except InvalidCredentialsError as creds:
            print("ERROR: invalid account: ", self.login_account, file=sys.stderr)
            sys.exit()
        except ServerDownOrUnreachableError as err:
            print("ERROR: server not reachable or doesn't support Redfish.\n", err, file=sys.stderr)
            sys.exit()

        return self.connection

    def logout(self):
        if (self.connection):
            self.connection.logout()
        self.connection = None

    def parse_args(self, argv, callback=None):
        parser = argparse.ArgumentParser()
        parser.add_argument("-u", "--url", help="iLO URL")
        parser.add_argument("-m", "--mlx", help="Mellanox URL")
        parser.add_argument("-a", "--account", help="iLO Account")
        parser.add_argument("-p", "--password", help="iLO Password")
        if (callback):
            callback(parser)
        args = parser.parse_args()

        self.system_url = args.url
        self.login_account = args.account
        self.login_password = args.password

        return args


def add_endp(parser):
    parser.add_argument("-r", "--raw", action='store_true',
                        help="Output raw JSON")
    parser.add_argument("endpoint", nargs='?', help="iLO Endpoint")


if __name__ == "__main__":
    endp = "/redfish/v1/systems/1"

    client = Config()
    args = client.parse_args(sys.argv, add_endp)
    if (args.endpoint):
        endp = args.endpoint

    REDFISH_OBJ = client.get_client()
    RESPONSE = REDFISH_OBJ.get(endp)

    # Print out the response
    if args.raw:
        print(RESPONSE.read)
    else:
        print(json.dumps(RESPONSE.dict, indent=4, sort_keys=True))

    client.logout()
