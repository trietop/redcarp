# Capturing system metrics

## Installation

The `ilorest_chif.so` ([download from HP](https://github.com/HewlettPackard/python-ilorest-library#requirements)) library is required to be in the either **same path** where you run the script or in a path found by `LD_LIBRARY_PATH`.  The Mellanox tools also require that the `mft-4.13.3-6-x86_64.rpm` (or later) [package](https://www.mellanox.com/products/adapter-software/firmware-tools) be installed and started so the `mget_temp` and `mlxlink` commands are available.

The following Python3 packages are used: `jsonpath-ng`, `python-ilorest-library`, `flask`, `requests`

Install: `pip3 install -r requirements.txt`

> **All Scripts must run as `root` when collecting metrics locally.**

## Scripts

* `config.py` is a general purpose script and can be used to capture any iLO output data
* `capture.py` returns all data available from the system as a JSON object
* `capturecsv.py` renders the JSON data in a reduced format with Mellanox Devices, Temperature Sensors, and Fan Speeds (as a % of max)

> The other get_*.py scripts may be run individually to return individual blocks of information.

## Common Arguments

> If no connection parameters are provided, the capture scripts will run against the local host.  The `ilorest_chif.so` library must be installed for local access.

> For remote access, make sure the Mellanox temperature collector is serving, iLO user accounts are configured, and local values are returning the expected data.

* `--url` : iLO Endpoint (https://ilo.host)
* `--mlx` : Mellanox Endpoint (http://os.host)
* `--account` : User account
* `--password` : Password for access

For all other scripts `--help` will provide you a listing of required and optional parameters.

## Scripts & Example Usage

### `get_ib_temp.py`

Capture the Infiniband card temperature.  Run locally or serve over HTTP.  Make sure that the Mellanox `mft` package is installed.

Local capture:

```console
user$ sudo python3 get_ib_temp.py
```

Serve data:

```console
user$ sudo python3 get_ib_temp.py -s
```

> Change port (--port) or bind address (--bind) to restrict access

### `capture.py` as `redcarp`

Capture all of the data avialable for Mellanox, Temperature, and Fans within the system.

#### Usage

Display the information as formatted JSON:

```console
user$ sudo python3 capture.py -f
```

Capture the JSON in files (stored within /tmp) every second until stopped:

```console
user$ sudo python3 capture.py --collect -o /tmp -r 1
```

Capture remotely:

```console
user$ sudo python3 capture.py --collect -o /tmp -r 1 --url https://ilo.host --mlx http://os.host:9080
```

### `capturecsv.py` as `redcsv`

Produce a summarized CSV version of the system metrics.  The header values are (mellanox devices), (temperature sensors), and (fan speeds as percentages of max).

#### `capturecsv.py` Usage

Collect values every second:

```console
user$ sudo python3 capturecsv.py --collect
```

> Use `-r 0` to capture values as fast as possible from the system.

### `config.py`

The `config.py` script is a general purpose accessor script that will display the raw JSON returned from the iLO interface.  This script can be used in conjunction with `get_resource_directory.py` to interrogate the various metrics available within the system.

#### `config.py` Usage

Display the default system information:

```console
user$ sudo python3 config.py
```

Use a path from `get_resource_directory.py` to retrieve Power data:

```console
user$ sudo python3 config.py /redfish/v1/Chassis/1/Power/
```

### `get_resource_directory.py`

Return the available endpoints for resources within the system.  Use with `config.py`.

#### `get_resource_directory.py` Usage

```console
user$ sudo python3 get_resource_directory.py
```

## Notes

* The `get_ib_temp.py` uses the mget_temp utility to read the hardware temperature from Mellanox Technologies devices with temperature sensors (all Mellanox devices), and prints the result in Celsius degrees.  The mget_temp utility reads the IC temperature, it does not support reading temperature from peripheral sensors on the board.
