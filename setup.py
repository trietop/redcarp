import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="redcarp",
    version="0.0.1",
    author="Damon Brown",
    author_email="dbrown@trietop.com",
    description="Redfish Metrics Capture",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/trietop/redcarp",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.4',
    entry_points={
        'console_scripts': [
            'redcarp = redcarp:as_json',
            'redcsv = redcarp:as_csv',
            'redcarp-resources = redcarp:list_resources',
            'redcarp-mlx = redcarp:capture_mlx',
            'redcarp-power = redcarp:capture_power',
            'redcarp-thermal = redcarp:capture_thermal',
        ],
    },
)
